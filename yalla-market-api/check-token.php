<?php

require 'vendor/autoload.php'; // Include Composer's autoloader
require 'require.php'; 

header("Content-Type: application/json");

// Get the request method
$method = $_SERVER['REQUEST_METHOD'];

// Routing based on the request method
switch ($method) {
    case 'POST':
        checkAPI();
        break;
    default:
        respond(405, ["message" => "Method Not Allowed"]);
        break;
}

// Function to handle POST requests
function checkAPI() {
    try {
        
        $client = new \GuzzleHttp\Client();
        
        $request_body = json_decode(file_get_contents('php://input'), true);
        $token = $request_body["api_key"];

        if (empty( $request_body["api_key"] )) {
            echo json_encode(["httpCode" => 404, "status"=>false, "message" => "Token not define"]);
            die;
        }

        if (empty( $request_body["merchant_id"] )) {
            echo json_encode(["httpCode" => 404, "status"=>false, "message" => "Token not define"]);
            die;
        }   

        $url = CLOVER_URL . "merchants/" . $request_body["merchant_id"];
        $response = $client->request('GET', $url, [
            'headers' => [
                    'accept' => 'application/json',
                    'authorization' => "Bearer $token",
                ],
            ]
        );      

        $merchant_data = [];

        if ($response->getStatusCode() == 200) {
            $response = json_decode($response->getBody()->getContents());
           
            if(!empty($response->name)){
                $merchant_data["name"] = $response->name;
                
                // Get Merchant Address
                $url = CLOVER_URL . "merchants/" . $request_body["merchant_id"] . "/address";   
                $response = $client->request('GET', $url, [
                    'headers' => [
                            'accept' => 'application/json',
                            'authorization' => "Bearer $token",
                        ],
                    ]
                );        

                if ($response->getStatusCode() == 200) {
                    $response = json_decode($response->getBody()->getContents(),true);
                    unset($response["href"]);
                    $merchant_data["address"] = implode(" ",$response);
                    $merchant_data["status"] = "success";
                    echo json_encode(["httpCode" => 200, "status"=>true, "message" => "Merchant Data", "responseContent" => json_encode($merchant_data)]);
                    die;

                }else {
                    echo json_encode(["httpCode" => 404, "status"=>false, "message" => "Error: Received status code " . $response->getStatusCode()]);
                    die;
                }

            }else{
                echo json_encode(["httpCode" => 404, "status"=>false, "message" => "Error: Merchant not found"]);
                die;
            }

        }else {
            echo json_encode(["httpCode" => 404, "status"=>false, "message" => "Error: Received status code " . $response->getStatusCode()]);
            die;
        }
        
        


    } catch (\Throwable $th) {
        echo json_encode(["httpCode" => 500, "status"=>false, "message" => $th->getMessage()]);
        die;
    }

}

?>