<?php

require '../vendor/autoload.php'; // Include Composer's autoloader
require '../require.php'; 

header("Content-Type: application/json");

// Get the request method
$method = $_SERVER['REQUEST_METHOD'];
// Routing based on the request method
switch ($method) {
    case 'GET':        
        getCategories();
        break;
    default:
        echo json_encode(["httpCode" => 405, "status"=>false, "message" => "Method Not Allowed"]);
        die;
        break;
}

// Function to handle POST requests
function getCategories() {
    try {

        
        $client = new \GuzzleHttp\Client();
        
        $request_body = $_GET;
        $headers = getallheaders();

        // Check for the X-Authorization header
        if (empty($headers['X-Authorization']) || empty($headers['X-Authorization-merchant'])) {
            echo json_encode(["httpCode" => 404, "status"=>false, "message" => "X-Authorization header not defined"]);
            die;
        }

        $token = $headers['X-Authorization'];
        $merchant_id = $headers['X-Authorization-merchant'];

        if (empty( $token )) {
            echo json_encode(["httpCode" => 404, "status"=>false, "message" => "Token not define"]);
            die;
        }

        if (empty( $merchant_id )) {
            echo json_encode(["httpCode" => 404, "status"=>false, "message" => "Token not define"]);
            die;
        }   

        $url = CLOVER_URL . "merchants/" . $merchant_id . "/categories";
        $response = $client->request('GET', $url, [
            'headers' => [
                    'accept' => 'application/json',
                    'authorization' => "Bearer $token",
                ],
            ]
        );  

        $category_data = [];

        if ($response->getStatusCode() == 200) {
            $response = json_decode($response->getBody()->getContents());

            
            if(!empty($response->elements)){
                
                foreach($response->elements as $category){

                    $tmp = [];
                    $tmp['id'] = $category->id ?? "";
                    $tmp['name'] = $category->name ?? "";
                    $tmp['sortOrder'] = $category->sortOrder ?? "";
                    $tmp['items'] = [];
                    $tmp['colorCode'] = $category->colorCode ?? "";
                    $tmp['deleted'] = $category->deleted ?? "";
                    $tmp['modifiedTime'] = $category->modifiedTime ?? "";
                    $tmp['canonical'] = $category->canonical ?? "";
                    $tmp['menuSection'] = $category->menuSection ?? "";


                    $category_data[] = $tmp;
                }

                echo json_encode(["httpCode" => 200, "status"=>true, "message" => "Categories Data", "responseContent" => json_encode($category_data)]);
                die;

            }else{
                echo json_encode(["httpCode" => 404, "status"=>false, "message" => "Error: Category not found"]);
                die;
            }

        }else {
            echo json_encode(["httpCode" => 404, "status"=>false, "message" => "Error: Received status code " . $response->getStatusCode()]);
            die;
        }
        
        


    } catch (\Throwable $th) {
        echo json_encode(["httpCode" => 500, "status"=>false, "message" => $th->getMessage()]);
        die;
    }

}

?>